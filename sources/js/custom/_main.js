String.prototype.substrLengh = function(limit, postfix) {
	postfix = postfix || '...';
	var text = this.trim();
	if( text.length <= limit) return text;
	text = text.slice( 0, limit);
	var lastSpace = text.lastIndexOf(' ');
	if( lastSpace > 0) text = text.substr(0, lastSpace);
	return text + postfix;
};

Number.prototype.substrLengh = function(limit, postfix) {
	postfix = postfix|| '...';
	var text = this.toString().trim();
	if( text.length <= limit) return text;
	text = text.slice( 0, limit);
	var lastSpace = text.lastIndexOf(' ');
	if( lastSpace > 0) text = text.substr(0, lastSpace);
	return text + postfix;
};


(function(){
	var app = {
		module: {

			setScrollPosition: {

				timer: null,

				pos: function ($el, options) {

					if ( self.viewPort.current == 0 ) return false;

					$el.scrollToFixed(options);
				},

				init: function (el, options) {

					var $el = $(el);

					if ( $el.length < 1 ) return false;

					$el.each(function(){
						var $t = $(this);
						var timer = null;


						self.setScrollPosition.pos($t, options);

						$(window).resize(function () {
							if (timer) clearTimeout(timer);
							timer = setTimeout(function(){
								$t.trigger('detach.ScrollToFixed');
								$t.attr('style','');
								self.setScrollPosition.pos($t, options);
							},310);
						});
					});
				}
			},

			scrollMenu: {

				onScroll: function(){
					var scrollPos = $(document).scrollTop();

					$('[data-part]').each(function () {

						$.urlParam = function(name){
							var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
							if (results==null){
								return null;
							}
							else{
								return results[1] || 0;
							}
						};

						var $t = $(this),
							group = $t.data('part').match(/([\S]+#)/ig) ? $t.data('part').match(/([\S]+#)/ig)[0].replace('#','') : '';
							$section = $($t.data('part').match(/(#[\S]+)/ig)[0]);

						if ( $section.length && ($section.offset().top - 5 <= scrollPos ) &&  ( $section.offset().top - 5 + $section.height() > scrollPos )) {
							$('[data-part ^= ' + group + ']').removeClass('is-active');
							$t.addClass('is-active');

						}
						else{
							$t.removeClass('is-active');
						}

					});
				},

				init: function(){
					$(window).on('scroll', function(){
						if ( self.viewPort.current !== 0 ) self.scrollMenu.onScroll();
					});

					if ( self.viewPort.current !== 0 ) self.scrollMenu.onScroll();
				}
			},

			checkForm: function(form){
				var $el = $(form),
					wrong = false;

				$el.find('[data-required]:visible').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form__field'),
						$mes = $wrap.find('.form__field-message'),
						val = $.trim($t.val()),
						errMes = '',
						rexp = /.+$/igm;

					$wrap.removeClass('error success');
					$mes.html('');

					if ( $t.attr('type') == 'checkbox' && !$t.is(':checked') ) {
						val = false;
					} else if ( /^(#|\.)/.test(type) ){
						if ( val !== $(type).val() || !val ) val = false;
					} else if ( /^(name=)/.test(type) ){
						if ( val !== $('['+type+']').val() || $.trim(val) == '' ) {
							val = $.trim(val) == '' ? '' : false;
							errMes = 'Значения полей должно совпадать';
						}
					} else if ( $t.attr('type') == 'radio'){
						var name =  $t.attr('name');
						if ( $('input[name='+name+']:checked').length < 1 ) val = false;
					} else {
						switch (type) {
							case 'number':
								rexp = /^\d+$/i;
								errMes = 'Поле должно содержать только числовые символы';
								break;
							case 'phone':
								rexp = /[\+]\d\s[\(]\d{3}[\)]\s\d{3}\s\d{2}\s\d{2}/i;
								break;
							case 'letter':
								rexp = /^[A-zА-яЁё]+$/i;
								break;
							case 'rus':
								rexp = /^[А-яЁё]+$/i;
								break;
							case 'email':
								rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
								errMes = 'Проверьте корректность email';
								break;
							// case 'password':
							// 	rexp = /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
							// 	errMes = 'Слишком простой пароль';
							// 	break;
							default:
								rexp = /.+$/igm;
						}
					}

					if ( !rexp.test(val) || val == 'false' || !val ){
						wrong = true;
						$wrap.addClass('error');

						if ($.trim(val) == '') errMes = 'Это поле необходимо заполнить';

						if ( errMes ){
							$mes.html(errMes);
						}

					} else {
						$wrap.addClass('success');

					}

				});


				return !wrong;

			},

			rangeSlider: {

				init: function(){

					var $slider = $('.filter__range-item');

					$slider.each(function(){
						var $t = $(this),
							$wrap = $t.closest('.filter__range'),
							slider = $t[0];

						noUiSlider.create(slider, {
							start: [$t.data('val-start'), $t.data('val-end')],
							margin: 1,
							step: 1,
							animate: true,
							connect: true,
							range: {
								'min': $t.data('val-min'),
								'max': $t.data('val-max')
							}
						});

						slider.noUiSlider.on('change', function ( values, handle ) {
							var val = values;
							val[handle] = (Math.round(values[handle]));
							slider.noUiSlider.set(val);
							$wrap.find('.i-from').val(Math.round(val[0]));
							$wrap.find('.i-to').val(Math.round(val[1]));

							setValue($wrap, values);
						});

						slider.noUiSlider.on('slide', function ( values, handle ) {
							setValue($wrap, values);
						});

						$wrap.find('.i-from').val(Math.round($t.data('val-start')));
						$wrap.find('.i-to').val(Math.round($t.data('val-end')));

						setValue($wrap, [$t.data('val-start'), $t.data('val-end')]);

					});

					$('.filter__range .i-from, .filter__range .i-to').on('blur keyup', function(){
						var $t = $(this),
							$wrap = $t.closest('.filter__range'),
							$slider = $wrap.find('.filter__range-item'),
							$from = $wrap.find('.i-from'),
							$to = $wrap.find('.i-to'),
							min = $slider.data('val-min'),
							max = $slider.data('val-max');

						if ( $t.val().match(/[^0-9]/g) ) {
							var _newVal = $t.val().replace(/[^0-9]/g, '');
							$t.val(_newVal);
						}

						$slider[0].noUiSlider.set([$from.val(), $to.val()]);

					}).on('blur change', function(){
						var $t = $(this),
							$wrap = $t.closest('.filter__range'),
							$slider = $wrap.find('.filter__range-item'),
							$from = $wrap.find('.i-from'),
							$to = $wrap.find('.i-to'),
							min = $slider.data('val-min'),
							max = $slider.data('val-max');

						if ( $t.hasClass('i-from') ){
							if (+$t.val() > +$to.val()) $t.val(+$to.val()-1);
							if (!$t.val() || +$t.val() < min) $t.val(min);
						} else {
							if (+$t.val() < +$from.val()) $t.val(+$from.val()+1);
							if (!$t.val() || +$t.val() > max) $t.val(max);
						}

						$slider[0].noUiSlider.set([$from.val(), $to.val()]);

					});


					function setValue($wrap, val){
						$wrap.find('.filter__range-value').html(Math.round(val[0]) + ' - ' + Math.round(val[1]) + ' <i class="rub"></i>');
					}

				}
			},

			viewPort: {

				current: '',

				data: {
					'0': function(){
						self.viewPort.current = 0;

						self.scrollBar.destroy('.master-photo__slider-list');
					},
					'1280': function(){
						self.viewPort.current = 1;

						self.scrollBar.init('.master-photo__slider-list', {
							axis:"x",
							theme:"light-3",
							advanced:{autoExpandHorizontalScroll:true}
						});
					},
					'1600': function(){
						self.viewPort.current = 2;

						self.scrollBar.init('.master-photo__slider-list', {
							axis:"x",
							theme:"light-3",
							advanced:{autoExpandHorizontalScroll:true}
						});
					}
				},

				init: function(data){
					var points = data || self.viewPort.data;
					if ( points ){
						points['Infinity'] = null;
						var sbw = scrollBarWidth(), curPoint = null;
						var ww = $(window).width() + sbw;
						checkCurrentViewport();
						$(window).on('resize', function(){
							ww = $(window).width() + sbw;
							checkCurrentViewport();
						});
					}

					function checkCurrentViewport(){
						var pp = 0, pc = null;
						$.each(points, function(point, callback){
							if ( point > ww ){
								if ( pp !== curPoint ) {
									curPoint = pp;
									pc();
								}
								return false;
							}
							pp = point; pc = callback;
						});
					}

					function scrollBarWidth(){
						var scrollDiv = document.createElement('div');
						scrollDiv.className = 'scroll_bar_measure';
						$(scrollDiv).css({
							width: '100px',
							height: '100px',
							overflow: 'scroll',
							position: 'absolute',
							top: '-9999px'
						});
						document.body.appendChild(scrollDiv);
						sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
						document.body.removeChild(scrollDiv);
						return sbw;
					}

				}
			},

			tooltip: {
				init: function(){
					var $tooltip = $('.tooltip');

					if ( !$tooltip.size() ){
						$tooltip = $('<div/>', {'class': 'tooltip'}).appendTo('body');
					}

					$('body').on('mouseover', '[data-tooltip]', function(){
						var $t = $(this);
						var text = $t.attr('data-tooltip');
						$tooltip.html('<span class="tooltip-inner">'+text+'</span>').stop(true, true).delay(1000).addClass('show');

						var top = $t.offset().top + $t.outerHeight();
						var left = $t.offset().left + ($t.outerWidth()/2);
						$tooltip.css({top: top, left: left});

					}).on('mouseleave', '[data-tooltip]', function(){
						$tooltip.removeClass('show');
					});


				}
			},

			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup__inner">'
						+				'<div class="popup__layout">'
						+					'<div class="popup__close"></div>'
						+					'<div class="popup__content"></div>'
						+				'</div>'
						+			'</div>'
						+			'<div class="popup__overlay"></div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					var $popup = $(popup);

					if ( !$popup.size() ){
						$popup = self.popup.create(popup);
					}

					if( html ){
						$popup.find('.popup__content').html(html);
					}
					$('body').addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(mes, okbtn){
					var html = '<div class="popup__content-title">' + mes + '</div>';
					if (okbtn) html += '<div class="popup__link"><span class="btn js-popup-close">Ок</span></div>';
					self.popup.open('.popup_info', html);
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.remove();
				},

				init: function(){
					$('body')
						/*.on('click', '.popup', function(e){
							if ( !$(e.target).closest('.popup__layout').size() ) self.popup.close('.popup');
						})*/
						.on('click', '.popup__close, .js-popup-close', function(e){
							e.preventDefault();
							e.stopPropagation();
							self.popup.close($(this).closest('.popup'));
						})
						.on('click', '[data-popup]', function(e){
							e.preventDefault();
							e.stopPropagation();
							self.popup.close('.popup');
							self.popup.open($(this).data('popup'));
						})
					;

					if ( $.urlParam('showPopup') ){
						self.popup.open($.urlParam('showPopup'));
					}

				}
			},

			mobileMenuDim: {
				init: function(el){
					var winWidth = $(document).width();
					var winHeight = winWidth * $(window).height() / $(window).width();
				alert(winWidth +', '+winHeight );
					$(el).css({
						'width': winWidth,
						'height': winHeight
					});

					$(window).resize(function(){
						$(el).css({
							'width': winWidth,
							'height': winHeight
						});
					});
				}
			},

			scrollBar: {


				destroy: function(el){
					if ( $(el).hasClass('inited') ){
						$(el).filter('.inited').removeClass('inited').mCustomScrollbar('destroy');
					}
				},

				update: function(el){
					if ( $(el).hasClass('inited') ){
						$(el).mCustomScrollbar('update');
					}
				},

				init: function(el, options){

					options = options || {};

					options.callbacks = {
						onCreate: function(){
							$(this).addClass('inited');
						}
					};

					$(el).not('.inited').mCustomScrollbar(options);
				}
			},

			player: {

				init: function(){

					var $player = $('.player');

					$player.mediaelementplayer({
						alwaysShowControls: true,
						videoVolume: 'horizontal',
						features: ['playpause','progress','duration','volume','fullscreen']
					});
				}
			},

			goTo: function(el, time){
				$('html,body').animate({scrollTop: $(el).offset().top}, (time || 500));
			},

			tabs: {
				init: function(){
					$('body')
						.on('click', '[data-tab-link]', function(e){
							/**
							 * data-tab-link='name_1', data-tab-group='names'
							 * data-tab-targ='name_1', data-tab-group='names'
							 **/
							e.preventDefault();
							var $t = $(this);
							var group = $t.data('tab-group');
							var $links = $('[data-tab-link]').filter(selectGroup);
							var $tabs = $('[data-tab-targ]').filter(selectGroup);
							var ind = $t.data('tab-link');
							var $tabItem = $('[data-tab-targ='+ind+']').filter(selectGroup);

							if( !$t.hasClass('active')){
								$links.removeClass('active');
								$t.addClass('active');
								$tabs.fadeOut(150);
								setTimeout(function(){
									$tabs.removeClass('active');
									$tabItem.fadeIn(150, function(){
										$(this).addClass('active');
									});
								}, 150)
							}

							function selectGroup(){
								return $(this).data('tab-group') === group;
							}

						})
					;
				}
			},

			fileUploader: {

				isAdvancedUpload: function(){
					var div = document.createElement( 'div' );
					return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
				},

				add: function(val){
					return {
						image: '<div class="attachment__remove"></div><div class="image"></div><div class="attachment__progress"><i></i></div>',
						file: '<div class="attachment__val">' + val + '</div><div class="attachment__remove"></div><div class="attachment__progress"><i></i></div>'
					}
				},

				loadFile: function (e, action) {
					var $attachList = $('.attachment__list'),
						files = FileAPI.getFiles(e);

					console.info(e, files);

					FileAPI.each(files, function (file){

						var $attachItem = $('<div/>', {class: 'attachment__item is-loading'});


						// if ( file.size === 0 ){
							// self.popup.info('Это какой-то неправильный файл. Попробуйте выбрать другой.', true);
						// }
						if( file ){

							file.xhr = FileAPI.upload({
								url: action,
								files: { file: file },
								upload: function (e){

									$attachItem.addClass('is-progress');

									if( /^image/.test(file.type) ){

										$attachItem
											.addClass('attachment__item_image')
											.html(self.fileUploader.add()['image'])
										;

									} else {
										$attachItem
											.addClass('attachment__item_file')
											.attr('title', file.name)
											.html(self.fileUploader.add(file.name.substrLengh(20))['file'])
										;
									}

									$attachItem.appendTo($attachList);
								},

								progress: function (e){
									var pr = e.loaded/e.total * 100;
									$attachItem
										.find('.attachment__progress i')
										.css({'width': pr + '%'})
									;

								},

								complete: function (err, xhr){
									// console.info('complete');

									if ( err ) {
										self.popup.info(err + ': Чтото пошло не так с файлом ' + file.name + ' . Попробуйте загрузить другой файл.', true);
										$attachItem.remove();
									} else {
										var res = (JSON.parse(xhr.responseText));
										$attachItem
											.removeClass('is-loading is-progress')
											.find('.attachment__remove')
											.attr('data-file-id', res.fileId);

										if ( res.images && res.images.file && res.images.file.dataURL ){
											$attachItem.find('.image').css({'background-image': 'url('+ res.images.file.dataURL + ')'}).attr('data-src', res.images.file.dataURL);
										} else {

										}
									}

								}
							});
						}

					});

					FileAPI.reset(e.currentTarget);

				},

				init: function () {


					// $(document).on('paste', function (event) {
					// 	var clipboardData = event.clipboardData || event.originalEvent.clipboardData,
					// 		files = clipboardData.items,
					// 		action = $('[data-upload-action]').data('upload-action');
					// 	if (files.length > 0) self.fileUploader.loadFile(files, action);
					// });

					// document.onpaste = function(event){
					// 	var items = (event.clipboardData || event.originalEvent.clipboardData).items;
					//
					// 	for (index in items) {
					// 		var item = items[index];
					// 		if (item.kind === 'file') {
					// 			var blob = item.getAsFile();
					// 			var reader = new FileReader();
					// 			reader.onload = function(event){
					// 				console.log(event.target.result)}; // data url!
					// 			reader.readAsDataURL(blob);
					// 		}
					// 	}
					// }


					if ( self.fileUploader.isAdvancedUpload ){
						var $b = $('body'),
							dragBox = $('.dragbox');

						$b
							.on('dragenter', function (e) {
								e.stopPropagation();
								e.preventDefault();
								dragBox.addClass('is-show');
							})
							.on('dragover', function (e) {
								e.stopPropagation();
								e.preventDefault();
								dragBox.addClass('is-show');
							})
							.on('dragleave', function (e) {
								e.stopPropagation();
								e.preventDefault();

								if ( $(e.target).closest('.lk-chat').length < 1 ){
									dragBox.removeClass('is-show');
								}
							})
							.on('drop', function (e) {
								dragBox.removeClass('is-show');

								if ( $(e.target).closest('.dragbox_catch').length < 1 ){
									e.stopPropagation();
									e.preventDefault();
								}
							})
						;


						$('.dragbox_catch')
							.on('dragover', function (e) {
								e.stopPropagation();
								e.preventDefault();
								dragBox.addClass('is-active');
							})
							.on('dragleave', function (e) {
								e.stopPropagation();
								e.preventDefault();
								dragBox.removeClass('is-active');
							})
							.on('drop', function (e) {
								e.stopPropagation();
								e.preventDefault();

								dragBox.removeClass('is-show');
								dragBox.removeClass('is-active');

								var dataTransfer = e.dataTransfer || e.originalEvent.dataTransfer,
									files = dataTransfer.files;

								if (files.length > 0) self.fileUploader.loadFile(files, $(this).closest('form').data('upload-action'));
							})
						;
					}


					$('body')
						.on('change', '[name = attachment_files]', function(e) {
							self.fileUploader.loadFile(e, $(this).closest('form').data('upload-action'));
						})

						.on('click', '.attachment__remove', function(e) {
							var $t = $(this);

							$t.closest('.attachment__item').remove();
						})

						.on('click', '.js-reply-chat-message', function(e) {
							var $t = $(this),
								$checked = $('.chat__item-bubble').filter('.is-checked'),
								$attachList = $('.attachment__list'),
								$attachItem = $('<div/>', {class: 'attachment__item'});


							if ( $checked.length > 1 ){
								$attachItem.addClass('attachment__item_messages').html('<div class="attachment__val">' + $checked.length + ' сообщений</div><div class="attachment__remove"></div>');
							} else if ( $checked.length === 1 ){
								var $wrap = $checked.closest('.chat__item'),
									$author = $wrap.find('.chat__item-author').eq(0);

								$checked.removeClass('is-checked is-unread');

								$attachItem.addClass('attachment__item_message').html('<div class="chat__item chat__item_l "> <div class="chat__item-author">' + $author.html() + '</div> ' + $checked.html() +  '</div>' + '<div class="attachment__remove"></div>');
							}

							$('html,body').animate({scrollTop: $('.chat__send').offset().top}, 500);
							$checked.removeClass('is-checked');
							$('.js-chat-control').hide();
							$attachItem.appendTo($attachList);

						})

						.on('click', '.js-remove-chat-message', function(e) {
							var $t = $(this),
								$checked = $('.chat__item-bubble').filter('.is-checked');

							/**
							 * TODO сценарий удаления приложеного файла
							 */

							$checked.each(function () {
								$(this).remove();
							});

							$('.chat__item').each(function () {
								var $t = $(this), $bubble = $t.find('.chat__item-bubble');


								if ( $bubble.length <= 0 ){
									$t.remove();
								}

							});

							$('.js-chat-control').hide();

						})

						.on('click', '.attachment__item_image .image', function(e) {
							var $t = $(this),
								$wrap = $t.closest('.attachment__item')
							;

							if ( !$wrap.hasClass('is-loading') ){
								self.popup.info('<img src="' + $t.data('src') + '">');
							}

						})


				}

			}
		},


		init: function() {

			$.urlParam = function(name){
				var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
				if (results==null){
					return null;
				}
				else{
					return results[1] || 0;
				}
			};

			self.viewPort.init();
			self.player.init();
			self.rangeSlider.init();
			self.popup.init();
			self.fileUploader.init();


			$('.js-phone-mask').mask('+7 (999) 999-99-99');

			$('[data-mask]').each(function () {
				$(this).mask($(this).attr('data-mask'));
			});

			var $b = $('body');

			$b
				.on('click', '[data-scrollto]', function(e){
					e.preventDefault();
					var $el = $(this).data('scrollto');
					$('html,body').animate({scrollTop: $($el).offset().top}, 500);
				})

			;
		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();



