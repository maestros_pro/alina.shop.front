// Event listenr

$(function(){

	var timer = setInterval(function(){
		if ($.app === undefined) return false;
		clearInterval(timer);
		setTimeout(function(){eventLoader($.app)}, 0);
	}, 50);

	function eventLoader(self) {


		self.setScrollPosition.init('.page-nav', {
			zIndex: 50,
			marginTop: 0,
			//limit: $('#prodDelivery').offset().top - $('.product__menu').outerHeight() + 70,
			removeOffsets: false
		});

		self.setScrollPosition.init('.course-work__menu-list', {
			zIndex: 40,
			marginTop: 80,
			limit: function(){return $('.course-differences').offset().top + 90},
			removeOffsets: true
		});


		self.setScrollPosition.init('.master-video__advice .card', {
			zIndex: 40,
			marginTop: 80,
			limit: function(){return $('.footer').offset().top - $('.master-video__advice .card').height() },
			removeOffsets: true
		});

		/*self.setScrollPosition.init('.master-info__wrap', {
			zIndex: 40,
			marginTop: 80,
			limit: function(){return $('.footer').offset().top - $('.master-video__advice .card').height() },
			removeOffsets: true
		});*/

		self.scrollMenu.init();



		$('.master-photo__carousel-list')
			.on('init', function (e, slick) {
				$(this)
					.closest('.master-photo__carousel')
					.find('.master-photo__carousel-counter')
					.text('1/' + slick.$slides.length);
			})
			.on('afterChange', function (e, slick, currentSlide) {
				$(this)
					.closest('.master-photo__carousel')
					.find('.master-photo__carousel-counter')
					.text( (currentSlide + 1) + '/' + slick.$slides.length);

			})
			.slick();




		$('.course-work__advice .card__list')
			.on('init', function (slick) {
				var top = $(this).find('.slick-current').find('.card__advice-quote').outerHeight();
				$(this).find('.slick-arrow').css('top', top);
			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				var top = $(this).find('[data-slick-index=' + nextSlide + ']').find('.card__advice-quote').outerHeight();
				$(this).find('.slick-arrow').css('top', top);
			})
			.slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				touchMove: false,
				responsive: [
					{
						breakpoint: 1280,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});



		$('.course-slider__list')
			.slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 3,
				touchMove: false,
				responsive: [
					{
						breakpoint: 1280,
						settings: {
							slidesToShow: 1
						}
					}
				]
			});



		var $b = $('body');

		$b

			.on('click', function (e) {

				closest('.sort__search', 'is-open');
				closest('.sort__select', 'is-open');
				closest('.hint', 'hint_show');
				closest('.handle', 'handle_show');

				function closest(el, cls){
					if ( $(el + '.' + cls).size() ){
						if ( !$(e.target).closest(el + '.' + cls).size() ) $(el + '.' + cls).removeClass(cls);
					}
				}

			})



			.on('click', 'a', function (e) {
				var $t = $(this),
					reg = /^[\.#]/,
					hash = $t.attr('href');

				if ( reg.test(hash) && $(hash).length > 0){
					e.preventDefault();
					$('html,body').animate({scrollTop:$(hash).offset().top}, 500);
				}
			})

			.on('click', function (e) {
				if ( $('.header__control-user.is-open').size() ){
					if ( !$(e.target).closest('.header__control-user.is-open').size() ) $('.header__control-user.is-open').removeClass('is-open');
				}

				if ( $('.nav__share-item.is-open').size() ){
					if ( !$(e.target).closest('.nav__share-item.is-open').size() ) $('.nav__share-item.is-open').removeClass('is-open');
				}

			})

			.on('submit', '.js-form', function (e) {
				e.preventDefault();
				var $form = $(this);

				if ($.app.checkForm($form)) {

					$.ajax({
						type: $form.attr('method'),
						url: $form.attr('action'),
						data: $form.serialize(),
						complete: function (res) {

							var data = JSON.parse(res.responseText);

							if (res.status) {
								var status = res.status,
									formName = $form.data('form-name');

								if (status == 200) {    /** success */

									switch (formName) {
										case 'courseFilter':
											$form.closest('.show-filter').removeClass('show-filter');
											break;
										case 'feedback':

											break;

										case 'faq':

											break;
									}

								} else if (status == 400) {    /** error */

								} else if (status == 500) {    /** Internal Server Error */

								} else {
									/** other trouble */
									console.error(res);
								}

							}
						}
					});
				}

			})

			.on('click', '.course-faq__item .handle', function (e) {
				$(this)
					.closest('.course-faq__item')
					.find('.ansver').slideToggle(300, function () {
					})
					.closest('.course-faq__item')
					.toggleClass('is-open');
			})

			.on('click', '.pagenav__handle', function (e) {
				$(this)
					.closest('.pagenav')
					.toggleClass('is-open');
			})


			.on('click', '.page-nav .pagenav__filter', function () {
				$('.filter').toggleClass('show-filter');
			})

			.on('click', '.filter__close', function () {
				$('.filter').removeClass('show-filter');
			})

			.on('click', '.filter__control-clear', function () {
				$(this).closest('form')[0].reset();
			})

			.on('click', '.filter__control-confirm', function () {
				$(this).closest('form').submit();
			})

			.on('click', '.filter__list-title', function (e) {
				if ( $(this).closest('.is-disabled').size() ) return false;

				var $t = $(this),
					$wrap = $t.closest('.filter__list'),
					$list = $wrap.find('.filter__list-wrap');

				if ( !$t.hasClass('in-progress') ){
					$t.addClass('in-progress');

					$list.slideToggle(300, function(){
						$wrap.toggleClass('is-open').removeAttr('style');
						$t.removeClass('in-progress');
					});

				}

			})


			.on('click', '.header__menu', function (e) {
				$('html').toggleClass('nav-open');
			})

			.on('click', '.js-close-nav', function () {
				$('html')
					.removeClass('nav-open')
					.find('nav')
					.removeClass('subnav-open')
					.find('.nav-mobile__item.is-active')
					.removeClass('is-active');
			})

			.on('click', '.js-back-nav', function () {


				if ( $(this).parents('.nav-mobile__item').length <= 2 ){
					$(this).closest('nav').removeClass('subnav-open').find('.nav-mobile__item.is-active').removeClass('is-active');
				} else {
					$(this).closest('.nav-mobile__item.is-active').removeClass('is-active');
				}
			})

			.on('click', '.nav-mobile__link', function () {
				var $t = $(this),
					$nav = $t.siblings('.nav-mobile__subnav');

				if ($nav.size()) {
					$t.closest('.nav-mobile__item').addClass('is-active').closest('nav').addClass('subnav-open');
				}
			})





			.on('click', '.master-photo__slider-more .btn', function () {
				var $t = $(this),
					i = 0,
					$num = $t.find('.js-num'),
					l = +$num.text(),
					$itemHidden = $t.closest('.master-photo__slider').find('.master-photo__slider-item').filter('.is-hidden');
				if ( $itemHidden.length <= l ) $t.hide();
				for (i; i < l; i++) $itemHidden.eq(i).removeClass('is-hidden');
				if ( ( $itemHidden.length - l ) < l  ) $num.text($itemHidden.length - l);

			})

			.on('click', '.master-photo__slider-item', function () {
				var $t = $(this),
					ind = + $t.data('num') - 1;

				$('.master-photo__carousel-list').slick('slickGoTo', ind, true);
				if (self.viewPort.current == 0) self.goTo('.master-photo__carousel-list');

			})






			.on('click', '.filter__calendar-select-name', function () {

				if ( $(this).closest('.is-disabled').size() ) return false;

				 $(this).closest('.filter__calendar-select').toggleClass('is-open');

			})
			.on('click', '.filter__calendar-select-list .close', function () {
				 $(this).closest('.filter__calendar-select').removeClass('is-open');

			})



			.on('click', '.header__control-user', function () {
				 if ( !$(this).hasClass('is-open') ) $(this).toggleClass('is-open');

			})






			.on('click', '.nav__share-item', function (e) {

				if ( $(this).find('.nav__share-dropdown').size() ) {
					if ( !$(this).hasClass('is-open') ) $(this).toggleClass('is-open');
				}

			})



			.on('click', '.master-mega__head', function (e) {

				var $t = $(this),
					$wrap = $t.closest('.master-mega__group'),
					$list = $wrap.find('.master-mega__list');

				if ( !$t.hasClass('is-loading') && !$wrap.hasClass('is-disabled') ){
					$t.addClass('is-loading');

					$list.slideToggle(300, function(){
						$wrap.toggleClass('is-open');
						$t.removeClass('is-loading');
					});

				}

			})

			.on('click', '.course-legal__faq-head', function (e) {

				var $t = $(this),
					$wrap = $t.closest('.course-legal__faq-item'),
					$list = $wrap.find('.course-legal__faq-body');

				if ( !$t.hasClass('is-loading') && !$wrap.hasClass('is-disabled') ){
					$t.addClass('is-loading');

					$list.slideToggle(300, function(){
						$wrap.toggleClass('is-open');
						$t.removeClass('is-loading');
					});

				}

			})




			// lk

			.on('change', '.sort__select-item input', function () {
				var $t = $(this),
					$item = $t.closest('.sort__select-item'),
					$wrap = $t.closest('.sort__select');

				$wrap.removeClass('is-open').find('.sort__select-val .val').html($item.find('span').html());
			})


			.on('click', '.js-toggle-filter', function () {

				$b.toggleClass('show-filter');
			})

			.on('click', '.sort__select-val', function () {
				var $t = $(this),
					$wrap = $t.closest('.sort__select');

				$wrap.toggleClass('is-open');
				$('.sort__select').not($wrap).removeClass('is-open');

			})

			.on('click', '.lk-sort_dialog .sort__search', function () {
				var $t = $(this);

				$t.addClass('is-open');

			})

			.on('click', '.lk-dialog__item-menu-link_comment', function () {
				var $t = $(this),
					$wrap = $t.closest('.lk-dialog__item')
				;

				if ( $wrap.find('.chat__list').length ){

					$wrap.toggleClass('is-show-chat');

					if ( $wrap.hasClass('is-show-chat') ){
						$t.find('.n-m').html('Свернуть диалог');
					} else {
						$t.find('.n-m').html('Развернуть диалог');
					}
				}


			})



			.on('click', '.lk-chat .chat__list .chat__item-bubble', function (e) {
				var $t = $(this),
					$control = $('.js-chat-control'),
					timer = null;

				if ( $t.closest('.chat__item-attachment').size() ) {
					$t.closest('.chat__item-attachment').closest('.chat__item-bubble').click();
					return false;
				}

				if ( timer ) clearInterval(timer);

				$t.toggleClass('is-checked');
				$('.is-controllable').removeClass('is-controllable');

				var $isChecked = $('.chat__item-bubble.is-checked');

				if ( $isChecked.size() ){
					var $lastChecked = $isChecked.eq(-1);
					$lastChecked.addClass('is-controllable');

					$control.css({
						display: 'block',
						top: $isChecked.eq(-1).offset().top + $isChecked.eq(-1).height() - $control.parent().offset().top + 20
					});

					timer = setInterval(function () {
						$control.css({
							top: $isChecked.eq(-1).offset().top + $isChecked.eq(-1).height() - $control.parent().offset().top + 20
						});
					}, 10);

					setTimeout(function () {
						clearInterval(timer);
					}, 310);

				} else {
					$control.removeAttr('style');
				}
			})

			.on('mouseover', '.lk-chat .chat__list .chat__item-bubble', function () {
				var $t = $(this);
				$t.addClass('is-hover');
			})

			.on('mouseleave', '.lk-chat .chat__list .chat__item-bubble', function () {
				var $t = $(this);
				$t.removeClass('is-hover');
			})

			.on('keydown keyup input', '.js-autoheight', function () {
				var $el = $(this);

				var p = $el.outerHeight() - $el.height();

				//console.info($el.height(), $el.outerHeight(), $el[0].scrollHeight, $el.outerHeight() - $el.height());

				$el.css({
					height: 'auto'
					// padding: 0
				});
				$el.css({
					height: $el[0].scrollHeight
				});

			})


			// phone edit


			.on('click', '.lk-user__phone-edit', function () {
				var $t = $(this),
					$wrap = $t.closest('.lk-user__phone'),
					$field = $t.closest('.form__field'),
					$input = $field.find('input')
				;
				$input.prop('disabled', false).focus();

			})

			.on('click', '.lk-user__phone-add', function () {
				var $t = $(this),
					$wrap = $t.closest('.lk-user__phone'),
					$field = $t.closest('.form__field'),
					$input = $field.find('input'),
					fieldLength = $wrap.find('.form__field').size(),
					newName = 'phone[0]'
				;

				if ( !$input.val() ) {
					$field.addClass('error');
					setTimeout(function () {
						$field.removeClass('error');
					}, 1000)
				} else {
					var $clone = $field.clone().appendTo($wrap);

					for ( var i = 0; i < fieldLength; i++ ){
						var name = $wrap.find('.form__field').eq(i).find('input').attr('name');
						newName = 'phone[' + i + ']';

						if ( name === newName ) {
							newName = newName = 'phone[' + (i+1) + ']';
						}
					}

					$clone.removeClass('error').find('input').prop('disabled', false).val('').attr('name', newName).mask('+7 (999) 999-99-99').focus();
				}

			})

			.on('click', '.lk-user__phone-remove', function () {
				var $t = $(this),
					$wrap = $t.closest('.lk-user__phone'),
					$field = $t.closest('.form__field'),
					$input = $field.find('input'),
					fieldLength = $wrap.find('.form__field').size()
				;

				if ( fieldLength < 2 ) {
					$wrap.find('.form__field').addClass('error');
					setTimeout(function () {
						$field.removeClass('error');
					}, 1000)
				} else {
					$field.remove();
				}

			})


			// email edit
			.on('click', '.lk-user__email-edit', function () {
				var $t = $(this),
					$wrap = $t.closest('.lk-user__email'),
					$field = $t.closest('.form__field'),
					$input = $field.find('input')
				;
				$input.prop('disabled', false).focus();

			})

			.on('click', '.lk-user__email-add', function () {
				var $t = $(this),
					$wrap = $t.closest('.lk-user__email'),
					$field = $t.closest('.form__field'),
					$input = $field.find('input'),
					fieldLength = $wrap.find('.form__field').size(),
					newName = 'email[0]'
				;

				if ( !$input.val() ) {
					$field.addClass('error');
					setTimeout(function () {
						$field.removeClass('error');
					}, 1000)
				} else {
					var $clone = $field.clone().appendTo($wrap);

					for ( var i = 0; i < fieldLength; i++ ){
						var name = $wrap.find('.form__field').eq(i).find('input').attr('name');
						newName = 'email[' + i + ']';

						if ( name === newName ) {
							newName = newName = 'email[' + (i+1) + ']';
						}
					}

					$clone.removeClass('error').find('input').prop('disabled', false).val('').attr('name', newName).focus();
				}

			})

			.on('click', '.lk-user__email-remove', function () {
				var $t = $(this),
					$wrap = $t.closest('.lk-user__email'),
					$field = $t.closest('.form__field'),
					$input = $field.find('input'),
					fieldLength = $wrap.find('.form__field').size()
				;

				if ( fieldLength < 2 ) {
					$wrap.find('.form__field').addClass('error');
					setTimeout(function () {
						$field.removeClass('error');
					}, 1000)
				} else {
					$field.remove();
				}

			})

			.on('click', '.lk-user__avatar-clear ', function () {
				var $t = $(this),
					$wrap = $t.closest('.form__field'),
					$preview = $wrap.find('.lk-user__avatar-preview'),
					$input = $wrap.find('[name=avatar]')
				;

				$preview.html('');
				$input.val('');

			})

			.on('click', '.js-avatar-crop', function () {
				var $cropper = $('#avatarCropper'),
					avatar = $cropper.cropper('getCroppedCanvas', { width: 500, height: 500 }),
					src = avatar.toDataURL('image/jpeg', 0.9);

				$('input[name=avatar]').val(src);

				$('.lk-user__avatar-preview').html('<img src="' + src + '">');

				self.popup.close('.popup_avatar');
			})

			.on('change', '#avatarPreload', function (e) {

				var file, reader, img, $popup, $img;

				if ( this.value && !/\.(jpg|jpeg|png|gif)$/i.test( this.value ) ) {
					self.popup.info('Вы выбрали не верный тип файла<br>Допустимые типы файлов: jpg, jpeg, png', true);
				} else if (this.value && window.FileReader) {
					file = e.target.files[0];
					if ( file ){
						reader  = new FileReader();
						reader.readAsDataURL(file);
						reader.onloadend = function () {
							img = new Image();
							img.src = reader.result;

							img.onload = function(){
								$popup = self.popup.open('.popup_avatar', '<div class="popup__cropper"><img id="avatarCropper" src="' + img.src + '"></div><div class="popup__link"><span class="btn js-avatar-crop">Ок</span></div>');

								$img = $popup.find('img');

								$img.cropper({
									guides: false,
									aspectRatio: 100 / 100,
									dragMode: 'move',
									viewMode: 1,
									movable: true,
									cropBoxMovable: false,
									center: false,
									zoomable: true,
									restore: false,
									checkCrossOrigin: false,
									responsive: false,
									cropBoxResizable: false,
									toggleDragModeOnDblclick: false,
									minContainerHeight: 300,
									built: function(e){

										//console.info($(e.currentTarget.nextElementSibling).width());

										$(this).cropper('setCropBoxData', {
											'width': 300,
											'height': 300,
											'top': ($(e.currentTarget.nextElementSibling).height() - 300) / 2,
											'left': ($(e.currentTarget.nextElementSibling).width() - 300) / 2
										});
									}
								});
							};
						};
					}
				} else {
					self.popup.info('Ошибка File reader : ' + window.navigator, true);
				}

			})

			.on('change', '#userBirthDay', function (e) {

				var date = this.value.split('/'),
					$day = $('.lk-user__birthday-day'),
					$month = $('.lk-user__birthday-month'),
					$year = $('.lk-user__birthday-year'),
					months = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"];


				$day.text(date[0]);
				$month.text(months[+date[1] - 1]);
				$year.text(date[2]);
			})


			.on('click', '.hint__link', function (e) {
				$(this).closest('.hint').toggleClass('hint_show');
			})


			.on('click', '.handle__btn', function (e) {
				var $handle = $(this).closest('.handle');
				$('.handle').not($handle).removeClass('handle_show');
				$handle.toggleClass('handle_show');
			})





		;


		$('#userBirthDay').datepicker({
			format: "dd/mm/yyyy",
			weekStart: 1
		});

		$('.lk-user__birthday-edit').on('mousedown', function (e) {
			var $date = $('.datepicker');
			if ( $date.is(':visible') ) e.preventDefault();
		});



		$('.course-legal__form').each(function () {
			var $form = $(this),
				$input = $form.find('input'),
				$submit = $form.find('[type="submit"]')
			;

			$input.on('change', function (e) {
				var valid = true,
					$this = $(this)
				;

				$this.parent()
					.find('.course-legal__step-file-error')
					.html('')
				;

				if ( $(this).attr('type') === 'file' ){
					var file = e.target.files[0];

					if ( file && /^(image)/ig.test(file.type) && file.size <= 300000 ){
						var reader = new FileReader();
						reader.readAsDataURL(file);
						reader.onloadend = function(){
							$this.parent().find('.course-legal__step-file-image').html('<img src="' + reader.result + '">');
						};
					} else {
						if (file && file.size > 300000){
							self.popup.info('Размер файла не должен превышать 300кб', true);
						}
						$t.val('');
						return false;
					}
				}

				$input.each(function () {
					var $t = $(this);

					if ( $t.attr('type') === 'file' ){
						if ( !!$t.val() ){
							$t.closest('.course-legal__step-file').addClass('success');
						} else {
							$t.closest('.course-legal__step-file').removeClass('success');
							valid = false;
						}
					} else if ( $t.attr('type') === 'checkbox' ){
						if ( $t.prop('checked') ){

						} else {
							valid = false;
						}
					}
				});

				$submit.prop('disabled', !valid);
			});

			$form.
				on('submit', function (e) {
					e.preventDefault();

					if ( !$submit.attr('disabled') && $submit.hasClass('moderate') ) return false;

					$submit.attr('disabled', true).addClass('loading');

					var formData = new FormData($form[0]);
					$form.find('.course-legal__step-file-error').html('');


					try {
						for (var value of formData.values()) {
							console.log(value);
						}
					} catch(e) {}

					$.ajax({
						url: $form.attr('action'),
						type: $form.attr('method'),
						//enctype: 'multipart/form-data',
						data: formData,
						success: function (data) {
							if (data.errors && data.errors.length){
								for (var i = 0; i < data.errors.length; i++){
									$('[name="' + data.errors[i].name + '"]')
										.parent()
										.find('.course-legal__step-file-error')
										.html(data.errors[i].message)
									;
								}
							} else {
								$submit.attr('disabled', true).removeClass('loading').addClass('moderate').text('Документы на модерации');
								$form.find('input').attr('disabled', true);
								$form.find('.course-legal__step-file-load').hide();
								$form.find('.course-legal__step-file-remove').hide();
							}
						},
						cache: false,
						contentType: false,
						processData: false,
						error: function(){
							$submit.attr('disabled', false).removeClass('loading');
						}
					});

				})
				.on('click', '.course-legal__step-file-remove', function(e) {

					var $wrap = $(this).closest('.course-legal__step-file');

					$wrap.removeClass('.success');
					$wrap.find('.course-legal__step-file-image').html('');
					$wrap.find('input').val('').trigger('change');


				})

		});
	}
});